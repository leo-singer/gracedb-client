Installation
============
This section covers all of the currently available options for installing this package.


pip
---
.. code-block:: bash

    pip install ligo-gracedb


Conda
-----
See the instructions `here <https://anaconda.org/conda-forge/ligo-gracedb>`__ to install ligo-gracedb via conda.


Debian
------
Follow the instructions `here <https://wiki.ligo.org/Computing/DASWG/DebianJessie>`__ to configure the LSCSoft Debian repository.
The instructions are for Debian Jessie but are also applicable (with corresponding modifications) to Stretch and Buster.
After the repository is configured, you can install ligo-gracedb by doing::

    apt-get update
    apt-get install python-ligo-gracedb python3-ligo-gracedb


Scientific Linux 7
------------------
Follow the instructions `here <https://wiki.ligo.org/Computing/DASWG/ScientificLinux>`__ to configure the LSCSoft SL7 repository.
After the repository is configured, you can install ligo-gracedb by doing::

    yum update
    yum install python2-ligo-gracedb python34-ligo-gracedb
