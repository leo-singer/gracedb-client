Source: ligo-gracedb
Maintainer: Tanner Prestegard <tanner.prestegard@ligo.org>, Alexander Pace <alexander.pace@ligo.org>
Section: python
Priority: optional
Build-Depends: debhelper (>= 9), dh-python, python-all-dev, python3-all-dev, python-setuptools, python3-setuptools, python-six, python3-six, help2man
Standards-Version: 3.8.4
X-Python-Version: >=2.7
X-Python3-Version: >=3.5

Package: python-ligo-gracedb
Architecture: all
Depends: ${misc:Depends}, ${python:Depends}, python-ligo-common, python-future, python-setuptools, python-cryptography, python-requests
Provides: ${python:Provides}
Description: Gravitational-wave Candidate Event Database - Python
 The gravitational-wave candidate event database (GraceDB) is a prototype
 system to organize candidate events from gravitational-wave searches and to
 provide an environment to record information about follow-ups. A simple client
 tool is provided to submit a candidate event to the database.
 .
 This package provides Python support.

Package: python3-ligo-gracedb
Architecture: all
Depends: ${misc:Depends}, ${python3:Depends}, python3-ligo-common, python3-future, python3-setuptools, python3-cryptography, python3-requests
Provides: ${python3:Provides}
Description: Gravitational-wave Candidate Event Database - Python 3
 The gravitational-wave candidate event database (GraceDB) is a prototype
 system to organize candidate events from gravitational-wave searches and to
 provide an environment to record information about follow-ups. A simple client
 tool is provided to submit a candidate event to the database.
 .
 This package provides Python 3 support.
